# RPG Heroes - WeWereWarriors _(RPGHeroes)_

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/NicholasLennox/gradle-ci/badges/master/pipeline.svg)](https://gitlab.com/sondrem/RPGHeroes/-/commits/main)

RPG Heroes - A console application that create RPG characters built with Java17

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Test reports](#test-reports)
- [Built with](#built-with)
- [Contributing](#contributing)
- [License](#license)

## About
This project is meant to be part of a RPG game which creates characters called
heroes. In the game there are currently four classes that a hero can be:
- Mage
- Ranger
- Rogue
- Warrior

Each hero has the following shared fields:
- Name
- Level - all heroes start at level 1
- Hero Attributes
- Equipment - holds currently equipped items
- ValidWeaponTypes – a list of weapon types a hero can equip based on their class
- ValidArmorTypes - a list of armor types a hero can equip based on their class

The Hero Attribute system is based on three parameters: 
- Strength – determines the physical strength of the character.
- Dexterity – determines the characters ability to attack with speed and nimbleness.
- Intelligence – determines the characters affinity with magic.

Each type of hero class will start at different attributes and 
increase at different rates when levelling up.

Heroes can equip various items. The two types of items are: Weapon and Armor.
The equipped items will alter the power of the hero, causing it to deal more 
damage and be able to survive longer. Certain heroes can equip certain item
types.

## Install
Clone repository and compile all files. 

## Usage
Run WeAreWarriors.java.
At the moment the main method is empty but in the future it will
be filled with an interactive console-based gameplay loop with a
GameController for users to actually play the game.

At the moment unit testing is to verify the behavior of Heroes. 
This is how the assignment is "run". 

## Test reports 
Existing test report can be found. This is achieved with a gitlab-ci.yml file 
with the appropriate configuration and  a passing pipeline.

## Built with

- Java JDK 17
- JUnit5 - Test framework 
- Gradle - Dependency Management

## Contributing

PRs accepted.

## License

UNLICENSED