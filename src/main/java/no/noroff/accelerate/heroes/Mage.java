package no.noroff.accelerate.heroes;

import no.noroff.accelerate.utils.ArmorType;
import no.noroff.accelerate.utils.HeroAttribute;
import no.noroff.accelerate.utils.WeaponType;

public class Mage extends Hero {

    public Mage(String name) {
        super(name, new HeroAttribute(1,1,8));
        validWeaponTypes.add(WeaponType.STAFF);
        validWeaponTypes.add(WeaponType.WAND);
        validArmorTypes.add(ArmorType.CLOTH);
    }

    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes.increase(1, 1, 5);
    }

    @Override
    public int getDamageAttribute() {return totalIntelligence();}
}
