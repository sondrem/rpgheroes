package no.noroff.accelerate.heroes;

import no.noroff.accelerate.utils.ArmorType;
import no.noroff.accelerate.utils.HeroAttribute;
import no.noroff.accelerate.utils.WeaponType;

public class Warrior extends Hero {

    public Warrior(String name) {
        super(name, new HeroAttribute(5,2,1));
        validWeaponTypes.add(WeaponType.AXE);
        validWeaponTypes.add(WeaponType.HAMMER);
        validWeaponTypes.add(WeaponType.SWORD);
        validArmorTypes.add(ArmorType.MAIL);
        validArmorTypes.add(ArmorType.PLATE);
    }

    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes.increase(3, 2, 1);
    }

    @Override
    public int getDamageAttribute() {
        return totalStrength();
    }
}
