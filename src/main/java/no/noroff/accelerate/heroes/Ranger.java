package no.noroff.accelerate.heroes;

import no.noroff.accelerate.utils.ArmorType;
import no.noroff.accelerate.utils.HeroAttribute;
import no.noroff.accelerate.utils.WeaponType;

public class Ranger extends Hero {

    public Ranger(String name) {
        super(name, new HeroAttribute(1,7,1));
        validWeaponTypes.add(WeaponType.BOW);
        validArmorTypes.add(ArmorType.LEATHER);
        validArmorTypes.add(ArmorType.MAIL);
    }

    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes.increase(1, 5, 1);
    }

    @Override
    public int getDamageAttribute() {
        return totalDexterity();
    }
}
