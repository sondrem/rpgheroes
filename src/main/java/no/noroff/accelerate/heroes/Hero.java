package no.noroff.accelerate.heroes;

import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.items.Armor;
import no.noroff.accelerate.items.Item;
import no.noroff.accelerate.items.Weapon;
import no.noroff.accelerate.utils.ArmorType;
import no.noroff.accelerate.utils.HeroAttribute;
import no.noroff.accelerate.utils.Slot;
import no.noroff.accelerate.utils.WeaponType;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Hero {

    private final String name;
    protected int level;
    protected HeroAttribute levelAttributes;
    protected HashMap<Slot, Item> equipment;
    protected ArrayList<WeaponType> validWeaponTypes;
    protected ArrayList<ArmorType> validArmorTypes;

    public Hero(String name, HeroAttribute levelAttributes) {
        this.name = name;
        level = 1;
        this.levelAttributes = levelAttributes;
        equipment = new HashMap<>();
        equipment.put(Slot.WEAPON, null);
        equipment.put(Slot.HEAD, null);
        equipment.put(Slot.BODY, null);
        equipment.put(Slot.LEGS, null);
        validArmorTypes = new ArrayList<>();
        validWeaponTypes = new ArrayList<>();
    }

    /**
     * Method to levelUp hero. Increases level by 1.
     */
    public void levelUp() {
        level++;
    }


    /**
     * This method is called for equipping armor to a hero.
     * @param armor armor to be equipped.
     * @throws InvalidArmorException if the hero is a lower level than the
     * armors required level to equip or if it's not a valid armor type.
     */
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (level < armor.getRequiredLevel())
            throw new InvalidArmorException(name + " is too low level to equip this armor!");

        if (!validArmorTypes.contains(armor.getType()))
            throw new InvalidArmorException(name + " cannot equip this type of armor!");

        equipment.put(armor.getSlot(), armor);
    }

    /**
     * This method is called for equipping weapon to a hero.
     * @param weapon weapon to be equipped
     * @throws InvalidWeaponException if the hero is a lower level than the
     * weapon required level to equip or if it's not a valid weapontype.
     */
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (level < weapon.getRequiredLevel())
            throw new InvalidWeaponException(name + " is too low level to equip this weapon");

        if (!validWeaponTypes.contains(weapon.getType()))
            throw new InvalidWeaponException(name + " cannot equip this type of weapon");

        equipment.put(Slot.WEAPON, weapon);
    }


    /**
     * This method is called for showing the total damage for a hero, by the formula:
     * total damage = WeaponDamage * (1 + DamagingAttribute/100)
     * @return integer with the total damage for a hero.
     */
    public double damage() {
        double damageAttribute = getDamageAttribute();
        double weaponDamage = 1;
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
        if (weapon != null) weaponDamage = weapon.getWeaponDamage();

        return weaponDamage * (1+(damageAttribute/100));
    }

    /**
     * Method to get the damage attribute for a hero
     * @return an integer which is the damage attribute of the hero
     */
    public abstract int getDamageAttribute();


    /**
     * Method to calculate the total attributes of a hero, by the formula:
     * Total = LevelAttributes + (Sum of ArmorAttribute for all Armor in Equipment)
     * @return an integer which represents the total attributes of the hero
     */
    public int totalAttributes() {
        int sum = levelAttributes.getTotalAttributes();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = (Armor) equipment.get(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getTotalAttributes();
            }
        }
        return sum;
    }

    /**
     * Method to calculate the total strength of a hero
     * @return an integer which represents the total strength of a hero
     */
    public int totalStrength() {
        int sum = levelAttributes.getStrength();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = getArmor(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getStrength();
            }
        }
        return sum;
    }

    /**
     * Method to calculate the total dexterity of a hero
     * @return an integer which represents the total dexterity of a hero
     */
    public int totalDexterity() {
        int sum = levelAttributes.getDexterity();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = getArmor(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getDexterity();
            }
        }
        return sum;
    }

    /**
     * Method to calculate the total intelligence of a hero
     * @return an integer which represents the total intelligence of a hero
     */
    public int totalIntelligence() {
        int sum = levelAttributes.getIntelligence();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = getArmor(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getIntelligence();
            }
        }
        return sum;
    }

    /**
     * Method to display the follwing information about a hero:
     * Name
     * Class
     * Level
     * Total strength
     * Total dexterity
     * Total intelligence
     * Damage
     * @return a string with all information about a hero
     */
    public String display() {return this.toString();}

    @Override
    public String toString() {
        return "Your hero: {" +
                "Name='" + name + '\'' +
                ", Class=" + this.getClass().getSimpleName() +
                ", Level=" + level +
                ", Total strength=" + totalStrength() +
                ", Total dexterity=" + totalDexterity() +
                ", Total intelligence=" + totalIntelligence() +
                ", Damage=" + damage() + '}';
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }

    public Armor getArmor(Slot slot) {
        return (Armor) equipment.get(slot);
    }

    public Weapon getWeapon() {
        return (Weapon) equipment.get(Slot.WEAPON);
    }
}

