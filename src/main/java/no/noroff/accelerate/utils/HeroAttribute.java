package no.noroff.accelerate.utils;

import java.util.Objects;

public class HeroAttribute {
    private int strength, dexterity, intelligence;

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {return dexterity;}

    public int getIntelligence() {return intelligence;}

    /**
     * This method is called for retrieving all attributes.
     * @return an integer which represents the total attributes
     */
    public int getTotalAttributes() {
        return strength+dexterity+intelligence;
    }

    /**
     * This method is called for increasing the levels of the hero attribute
     * @param addedStrength strength to be added.
     * @param addedDexterity dexterity to be added.
     * @param addedIntelligence intelligence to be added.
     */
    public void increase(int addedStrength, int addedDexterity, int addedIntelligence) {
        strength += addedStrength;
        dexterity += addedDexterity;
        intelligence += addedIntelligence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeroAttribute that = (HeroAttribute) o;
        return strength == that.strength && dexterity == that.dexterity && intelligence == that.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength, dexterity, intelligence);
    }
}

