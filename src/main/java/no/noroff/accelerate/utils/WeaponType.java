package no.noroff.accelerate.utils;

public enum WeaponType {AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND}
