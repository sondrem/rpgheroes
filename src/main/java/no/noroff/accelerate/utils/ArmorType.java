package no.noroff.accelerate.utils;

public enum ArmorType {CLOTH, LEATHER, MAIL, PLATE}