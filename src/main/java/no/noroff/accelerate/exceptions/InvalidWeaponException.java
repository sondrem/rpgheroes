package no.noroff.accelerate.exceptions;

/**
 * Custom exception which is thrown if a hero tries to equip weapon
 *  and do not meet the weapons requirements.
 */
public class InvalidWeaponException extends Exception {
    public InvalidWeaponException(String message) {
        super(message);
    }
}