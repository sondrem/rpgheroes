package no.noroff.accelerate.exceptions;

/**
 * Custom exception which is thrown if a hero tries to equip armor
 * and do not meet the armors requirements.
 */
public class InvalidArmorException extends Exception {
    public InvalidArmorException(String message) {
        super(message);
    }
}
