package no.noroff.accelerate.heroes;

import no.noroff.accelerate.utils.HeroAttribute;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void rogueCreation_correctName_shouldPass() {
        //arrange
        String expected = "Peter";
        //act
        Rogue rogue = new Rogue("Peter");
        String actual = rogue.getName();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rogueCreation_correctLevel_shouldPass() {
        //arrange
        String name = "Peter";
        int expected = 1;
        //act
        Rogue rogue = new Rogue(name);
        int actual = rogue.getLevel();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rogueCreation_correctAttributes_shouldPass() {
        //arrange
        String name = "Peter";
        HeroAttribute expected = new HeroAttribute(2,6,1);
        //act
        Rogue rogue = new Rogue(name);
        HeroAttribute actual = rogue.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerLevelUp_validAttributes_shouldPass() {
        //arrange
        String name = "Peter";
        Rogue rogue= new Rogue(name);
        HeroAttribute expected = new HeroAttribute(3,10,2);
        //act
        rogue.levelUp();
        HeroAttribute actual = rogue.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rogueLevelUp_validLevel_shouldPass() {
        //arrange
        String name = "Peter";
        int expected = 2; //1+1
        Rogue rogue = new Rogue(name);
        //act
        rogue.levelUp();
        int actual = rogue.getLevel();
        //assert
        assertEquals(expected, actual);
    }
}