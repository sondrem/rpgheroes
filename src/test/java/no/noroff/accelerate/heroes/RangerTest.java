package no.noroff.accelerate.heroes;

import no.noroff.accelerate.utils.HeroAttribute;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void rangerCreation_correctName_shouldPass() {
        //arrange
        String expected = "Aragon";
        //act
        Ranger ranger = new Ranger("Aragon");
        String actual = ranger.getName();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerCreation_correctLevel_shouldPass() {
        //arrange
        String name = "Aragon";
        int expected = 1;
        //act
        Ranger ranger = new Ranger(name);
        int actual = ranger.getLevel();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerCreation_correctAttributes_shouldPass() {
        //arrange
        String name = "Aragon";
        HeroAttribute expected = new HeroAttribute(1,7,1);
        //act
        Ranger ranger = new Ranger(name);
        HeroAttribute actual = ranger.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerLevelUp_validAttributes_shouldPass() {
        //arrange
        String name = "Aragon";
        Ranger ranger= new Ranger(name);
        HeroAttribute expected = new HeroAttribute(2,12,2);
        //act
        ranger.levelUp();
        HeroAttribute actual = ranger.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerLevelUp_validLevel_shouldPass() {
        //arrange
        String name = "Aragon";
        int expected = 2; //1+1
        Ranger ranger = new Ranger(name);
        //act
        ranger.levelUp();
        int actual = ranger.getLevel();
        //assert
        assertEquals(expected, actual);
    }
}