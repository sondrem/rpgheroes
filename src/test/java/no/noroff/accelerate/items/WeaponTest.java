package no.noroff.accelerate.items;

import no.noroff.accelerate.utils.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    void weaponCreation_correctParamName_shouldPass() {
        //arrange
        String expected = "Common Axe";
        int requiredLevel = 1;
        WeaponType weaponType = WeaponType.AXE;
        int damage = 2;

        //act
        Weapon weapon = new Weapon(expected, requiredLevel, weaponType, damage);
        String actual = weapon.getName();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void weaponCreation_correctParamRequiredLevel_shouldPass() {
        //arrange
        String name = "Common Axe";
        int expected = 1;
        WeaponType weaponType = WeaponType.AXE;
        int damage = 2;

        //act
        Weapon weapon = new Weapon(name, expected, weaponType, damage);
        int actual = weapon.getRequiredLevel();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void weaponCreation_correctParamWeaponType_shouldPass() {
        //arrange
        String name = "Common Axe";
        int requiredLevel = 1;
        WeaponType expected = WeaponType.AXE;
        int damage = 2;

        //act
        Weapon weapon = new Weapon(name, requiredLevel, expected, damage);
        WeaponType actual = weapon.getType();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void weaponCreation_correctParamDamage_shouldPass() {
        //arrange
        String name = "Common Axe";
        int requiredLevel = 1;
        WeaponType weaponType = WeaponType.AXE;
        int expected = 2;

        //act
        Weapon weapon = new Weapon(name, requiredLevel, weaponType, expected);
        double actual = weapon.getWeaponDamage();

        //assert
        assertEquals(expected, actual);
    }
}