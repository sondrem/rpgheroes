package no.noroff.accelerate.items;

import no.noroff.accelerate.utils.ArmorType;
import no.noroff.accelerate.utils.HeroAttribute;
import no.noroff.accelerate.utils.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    void armorCreation_correctParamName_shouldPass() {
        //arrange
        int requiredLevel = 1;
        Slot slot = Slot.BODY;
        ArmorType type = ArmorType.PLATE;
        HeroAttribute heroAttribute = new HeroAttribute(1,0,0);
        String expected = "Common Plate Chest";

        //act
        Armor armor = new Armor("Common Plate Chest", requiredLevel, slot, type, heroAttribute);
        String actual = armor.getName();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void armorCreation_correctParamRequiredLevel_shouldPass() {
        //arrange
        String name = "Common Plate Chest";
        Slot slot = Slot.BODY;
        ArmorType type = ArmorType.PLATE;
        HeroAttribute heroAttribute = new HeroAttribute(1,0,0);
        int expected = 1;

        //act
        Armor armor = new Armor(name, 1, slot, type, heroAttribute);
        int actual = armor.getRequiredLevel();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void armorCreation_correctParamSlot_shouldPass() {
        //arrange
        String name = "Common Plate Chest";
        int requiredLevel = 1;
        ArmorType type = ArmorType.PLATE;
        HeroAttribute heroAttribute = new HeroAttribute(1,0,0);
        Slot expected = Slot.BODY;

        //act
        Armor armor = new Armor(name, requiredLevel, Slot.BODY, type, heroAttribute);
        Slot actual = armor.getSlot();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void armorCreation_correctParamType_shouldPass() {
        //arrange
        String name = "Common Plate Chest";
        int requiredLevel = 1;
        Slot slot = Slot.BODY;
        HeroAttribute heroAttribute = new HeroAttribute(1,0,0);
        ArmorType expected = ArmorType.PLATE;

        //act
        Armor armor = new Armor(name, requiredLevel, slot, ArmorType.PLATE, heroAttribute);
        ArmorType actual = armor.getType();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void armorCreation_correctParamHeroAttribute_shouldPass() {
        //arrange
        String name = "Common Plate Chest";
        int requiredLevel = 1;
        Slot slot = Slot.BODY;
        ArmorType type = ArmorType.PLATE;
        HeroAttribute expected = new HeroAttribute(1,0,0);

        //act
        Armor armor = new Armor(name, requiredLevel, slot, type, new HeroAttribute(1,0,0));
        HeroAttribute actual = armor.getArmorAttribute();

        //assert
        assertEquals(expected, actual);
    }



}